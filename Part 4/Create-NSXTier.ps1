#Requires -Version 4.0

<#  
.SYNOPSIS
    Creates a virtual network tier for VMware NSX with PowerNSX
.DESCRIPTION
    Creates a virtual network tier for VMware NSX with PowerNSX
.NOTES
    Author: Theo Hardendood, Metis IT B.V.

    Version History:
    2.0 - 18-02-2017 - Initial release
    Changes made in this version:
        Converted to PowerNSX
    1.0 - 05-04-2016 - Initial release
    Based on a script by Chris Wahl, @ChrisWahl, WahlNetwork.com
    Changes made on the original script:
        Replaced depricated code for handling certificate errors
        Fixed OSPF Routing
        Changed formatting of code to comply with my own coding standards
.PARAMETER NSXManager
	NSX Manager IP or FQDN
.PARAMETER NSXCredentials
	NSX Manager credentials with administrative authority
.PARAMETER vCenter
	vCenter Server IP or FQDN
.PARAMETER UseCurrentCredentials
    Use your current login credentials for vCenter
.PARAMETER vCenterCredentials
    vCenter credentials with administrative authority
.PARAMETER JSONPath
    Path to your JSON configuration file
.EXAMPLE
    PS> Create-NSXTier -NSXManager nsxmgr.tld -vCenter vcenter.tld -JSONPath "c:\path\prod.json"
#>

[CmdletBinding()]
Param
(
    [Parameter(Mandatory=$true,Position=0,HelpMessage="NSX Manager IP or FQDN")]
    [ValidateNotNullorEmpty()]
    [String]$NSXManager,
    [Parameter(Mandatory=$false,Position=1,HelpMessage="NSX Manager credentials with administrative authority")]
    [pscredential]$NSXCredentials,
    [Parameter(Mandatory=$true,Position=2,HelpMessage="vCenter Server IP or FQDN")]
    [ValidateNotNullorEmpty()]
    [String]$vCenter,
    [Parameter(Mandatory=$false,Position=3,HelpMessage="vCenter credentials with administrative authority")]
    [pscredential]$vCenterCredentials,
    [Parameter(Mandatory=$false,Position=4,HelpMessage="Use your current login credentials for vCenter")]
    [Switch]$UseCurrentCredentials,
    [Parameter(Mandatory=$true,Position=5,HelpMessage="Path to your JSON configuration file")]
    [ValidateNotNullorEmpty()]
    [String]$JSONPath
)

$ErrorActionPreference = "Stop"
try
{
    if ($global:DefaultNSXConnection -eq $null)
    {
        #
        # Get NSX Manager credentials
        #
        if ($NSXCredentials -eq $null)
        {
            $NSXCredentials = Get-StoredCredential -Name nsxadmin -Message "Enter NSX Manager credentials with administrative authority" -UserName "admin"
        }
        #
        # Get vCenter credentials
        #
        if ($vCenterCredentials -eq $null)
        {
            $vCenterCredentials = Get-StoredCredential -Name vcenter -Message "Enter vCenter credentials with administrative authority" -UserName "administrator@vsphere.local"
        }
        $dummy = Connect-NsxServer -Server $NSXManager -Credential $NSXCredentials -VICred $vCenterCredentials
    }
    #
    # Time this puppy!
    #
    $startClock = (Get-Date)
    #
    # Parse configuration from json file
    #
    try
    {
        $config = Get-Content -Raw -Path $JSONPath | ConvertFrom-Json
        Write-Host -BackgroundColor Black -ForegroundColor Yellow "Status: Parsed configuration from json file."
    }
    catch
    {
        throw "I don't have a config, something went wrong."
    }
    #
    # Combine switches and transit into a build list (easier than messing with a custom PS object!)
    #
    $switchlist = @()
    foreach ($switch in $config.switches)
    {
        $switchlist += $switch.name
    }
    $switchlist += $config.transit.name
    $transportZone = Get-NsxTransportZone
    if ($transportZone -eq $null)
    {
        throw "No network scope found. Create a transport zone and attach to your cluster."
    }
    #
    ###################
    # Create switches #
    ###################
    #
    # Get current list of switches to find if any already exist
    # Note: a switch is called a Virtual Wire in NSX
    #
    $result = $transportZone | Get-NsxLogicalSwitch
    $switches = @()
    foreach ($virtualWire in $result)
    {
        $switches += $virtualWire.name
    }
    #
    # Loop through our build list from earlier
    #
    foreach ($virtualWire in $switchlist)
    {
        #
        # Skip any duplicate switches
        #
        if ($switches.Contains($virtualWire))
        {
            Write-Host -BackgroundColor Black -ForegroundColor White "Warning: Logical Switch $virtualWire exists. Skipping."
        }
        else
        {
            #
            # Build any missing switch
            #
            $result = $transportZone | New-NsxLogicalSwitch -Name $virtualWire
            if ($result -eq $null)
            {
                throw "Was not able to create switch. API status description was not `"created`""
            }
            else
            {
                Write-Host -BackgroundColor Black -ForegroundColor Green "Status: Successfully created $virtualWire switch."
            }
        }
    }
    Write-Host -BackgroundColor Black -ForegroundColor Yellow "Status: Switch section completed."
    #
    #####################################
    # Create Distributed Logical Router #
    #####################################
    # Make sure the DLR doesn't already exist
    #
    $makeRouter = $true
    $routers = Get-NsxLogicalRouter
    foreach ($router in $routers)
    {
        if ($router.name -eq $config.router.name)
        {
            Write-Host -BackgroundColor Black -ForegroundColor White "Warning: DLR $($config.router.name) exists. Skipping."
            $makeRouter = $false
            $thisDLR = $router
            break
        }
    }
    #
    # If the $makeRouter flag is $false, skip the router part. Using it as a flag based on the previous router search.
    #
    If ($makeRouter -ne $false)
    {
        Write-Host -BackgroundColor Black -ForegroundColor Yellow "Status: Creating router. This may take a few minutes."
        #
        # Create the uplink interface
        #
        $internalInterfaces = @()
        $uplink = New-NsxLogicalRouterInterfaceSpec -Name $config.transit.name -Type uplink -ConnectedTo ($transportZone | Get-NsxLogicalSwitch $config.transit.name) -PrimaryAddress $config.transit.routerip -SubnetPrefixLength $config.transit.prefixlength
        $internalInterfaces = $internalInterfaces + $uplink
        # 
        # Create the internal interface(s) via a loop
        #
        foreach ($switch in $config.switches)
        {
            $internalInterface = New-NsxLogicalRouterInterfaceSpec -Name $switch.name -Type internal -ConnectedTo ($transportZone | Get-NsxLogicalSwitch $switch.name) -PrimaryAddress $switch.ip -SubnetPrefixLength $switch.prefixlength
            $internalInterfaces = $internalInterfaces + $internalInterface
        }
        #
        # Create router instance(s)
        #
        $cluster = Get-Cluster -Name $config.vsphere.cluster
        $enableHA = $config.router.ha -eq "true"
        $thisDLR = New-NsxLogicalRouter -Name $config.router.name -ManagementPortGroup (Get-VDPortgroup -Name $config.router.management.iface) -Interface $internalInterfaces -ResourcePool (Get-ResourcePool -Name $config.vsphere.resourcepool -Location $cluster) -EnableHA:$enableHA -Datastore (Get-DataStore -Name $config.vsphere.datastore)
        Write-Host -BackgroundColor Black -ForegroundColor Green "Status: Successfully created $($config.router.name) router."
        #
        # Routing configuration
        #
        $defaultGatewayvNic = $thisDLR | Get-NsxLogicalRouterInterface -Name $config.transit.name
        $dummy = $thisDLR | Get-NsxLogicalRouterRouting | Set-NsxLogicalRouterRouting -EnableOspf -ProtocolAddress $config.transit.protoip -ForwardingAddress $config.transit.routerip -RouterId $config.transit.routerip -EnableOspfRouteRedistribution -DefaultGatewayAddress $config.transit.edgeip -DefaultGatewayVnic $defaultGatewayvNic.index -Confirm:$false
        $thisDLR | Get-NsxLogicalRouterRouting | Get-NsxLogicalRouterOspfInterface | Remove-NsxLogicalRouterOspfInterface -Confirm:$false
        $thisDLR | Get-NsxLogicalRouterRouting | Get-NsxLogicalRouterOspfArea | Remove-NsxLogicalRouterOspfArea -Confirm:$false
        $area = Get-NsxLogicalRouter -Name $config.router.name | Get-NsxLogicalRouterRouting | New-NsxLogicalRouterOspfArea -AreaId $config.router.ospf.area -AuthenticationType none -Type normal -Confirm:$false
        $dummy = Get-NsxLogicalRouter -Name $config.router.name | Get-NsxLogicalRouterRouting | New-NsxLogicalRouterOspfInterface -AreaId $area.areaId -vNic $defaultGatewayvNic.index -confirm:$false
        Write-Host -BackgroundColor Black -ForegroundColor Green "Status: Successfully applied routing config to $($config.router.name)."
    }
    Write-Host -BackgroundColor Black -ForegroundColor Yellow "Status: Router section completed."
    #
    ###############
    # Create edge #
    ###############
    #
    # Make sure the edge doesn't already exist
    #
    $makeEdge = $true
    $edges = Get-NsxEdge
    foreach ($edge in $edges)
    {
        if ($edge.name -eq $config.edge.name)
        {
            Write-Host -BackgroundColor Black -ForegroundColor White "Warning: Edge $($config.edge.name) exists. Skipping."
            $makeEdge = $false
            $thisEdge = $edge
            break
        }
    }
    #
    # If the $makeEdge flag is $false, skip the edge part. Using it as a flag based on the previous router search.
    #
    If ($makeEdge -ne $false)
    {
        Write-Host -BackgroundColor Black -ForegroundColor Yellow "Status: Creating edge. This may take a few minutes."
        #
        # Create the uplink interface
        #
        $internalInterfaces = @()
        $uplink = New-NsxEdgeInterfaceSpec -Name $config.edge.uplink.name -Type uplink -ConnectedTo (Get-VDPortgroup -Name $config.edge.uplink.iface) -PrimaryAddress $config.edge.uplink.ip -SubnetPrefixLength $config.edge.uplink.prefixlength -Index 1
        $internalInterfaces = $internalInterfaces + $uplink
        $internalInterface = New-NsxEdgeInterfaceSpec -Name $config.transit.name -Type internal -ConnectedTo ($transportZone | Get-NsxLogicalSwitch $config.transit.name) -PrimaryAddress $config.transit.edgeip -SubnetPrefixLength $config.transit.prefixlength -Index 2
        $internalInterfaces = $internalInterfaces + $internalInterface
        #
        # Create edge instance(s)
        #
        $cluster = Get-Cluster -Name $config.vsphere.cluster
        $enableHA = $config.edge.ha -eq "true"
        $thisEdge = New-NsxEdge -Name $config.edge.name -Interface $internalInterfaces -ResourcePool (Get-ResourcePool -Name $config.vsphere.resourcepool -Location $cluster) -EnableHA:$enableHA -Datastore (Get-DataStore -Name $config.vsphere.datastore)
        Write-Host -BackgroundColor Black -ForegroundColor Green "Status: Successfully created $($config.edge.name) edge."
        #
        # Routing configuration
        #
        $defaultGatewayvNic = $thisEdge | Get-NsxEdgeInterface -Name $config.edge.uplink.name
        $dummy = $thisEdge | Get-NsxEdgeRouting | Set-NsxEdgeRouting -EnableOspf -RouterId $config.edge.uplink.ip -EnableOspfRouteRedistribution -DefaultGatewayAddress $config.edge.uplink.gateway -DefaultGatewayVnic $defaultGatewayvNic.index -Confirm:$false
        $thisEdge | Get-NsxEdgeRouting | Get-NsxEdgeOspfInterface | Remove-NsxEdgeOspfInterface -Confirm:$false
        $thisEdge | Get-NsxEdgeRouting | Get-NsxEdgeOspfArea | Remove-NsxEdgeOspfArea -Confirm:$false
        $thisEdge | Get-NsxEdgeRouting | Get-NsxEdgeRedistributionRule | Remove-NsxEdgeRedistributionRule -Confirm:$false
        $area0 = Get-NsxEdge -Name $config.edge.name | Get-NsxEdgeRouting | New-NsxEdgeOspfArea -AreaId 0 -AuthenticationType none -Type normal -Confirm:$false
        $dummy = Get-NsxEdge -Name $config.edge.name | Get-NsxEdgeRouting | New-NsxEdgeOspfInterface -AreaId $area0.areaId -vNic $defaultGatewayvNic.index -confirm:$false
        $transitvNic = $thisEdge | Get-NsxEdgeInterface -Name $config.transit.name
        $area = Get-NsxEdge -Name $config.edge.name | Get-NsxEdgeRouting | New-NsxEdgeOspfArea -AreaId $config.edge.ospf.area -AuthenticationType none -Type normal -Confirm:$false
        $dummy = Get-NsxEdge -Name $config.edge.name | Get-NsxEdgeRouting | New-NsxEdgeOspfInterface -AreaId $area.areaId -vNic $transitvNic.index -confirm:$false
        $dummy = Get-NsxEdge -Name $config.edge.name | Get-NsxEdgeRouting | New-NsxEdgeRedistributionRule -Learner ospf -FromConnected -Action permit -Confirm:$false
        Write-Host -BackgroundColor Black -ForegroundColor Green "Status: Successfully applied routing config to $($config.edge.name)."
    }
    Write-Host -BackgroundColor Black -ForegroundColor Yellow "Status: Edge section completed."
    #
    ############
    # Complete #
    ############
    #
    $endClock = (Get-Date)
    $totalClock = [Math]::Round(($endClock-$startClock).totalseconds)
    Write-Host -BackgroundColor Black -ForegroundColor Green "Status: Environment created in $totalclock seconds"
}
catch
{
    Write-Host -BackgroundColor Black -ForegroundColor Red "Status: A system exception was caught."
    Write-Host -BackgroundColor Black -ForegroundColor Red $Error[0]
}