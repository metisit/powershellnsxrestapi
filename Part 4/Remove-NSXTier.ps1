#Requires -Version 4.0

<#  
.SYNOPSIS
    Removes a virtual network tier in VMware NSX
.DESCRIPTION
    Removes a virtual network tier in VMware NSX
.NOTES
    Author: Theo Hardendood, Metis IT B.V.

    2.0 - 18-02-2017 - Initial release
    Changes made in this version:
        Converted to PowerNSX
    1.0 - 05-04-2016 - Initial release
    Based on a script by Chris Wahl, @ChrisWahl, WahlNetwork.com
    Changes made on the original script:
        Replaced depricated code for handling certificate errors
        Fixed OSPF Routing
        Changed formatting of code to comply with my own coding standards
.PARAMETER NSXManager
    NSX Manager IP or FQDN
.PARAMETER NSXCredentials
    NSX Manager credentials with administrative authority
.PARAMETER JSONPath
    Path to your JSON configuration file
.EXAMPLE
    PS> Remove-NSXTier -NSX nsxmgr.tld -JSONPath "c:\path\prod.json"
#>

[CmdletBinding()]
Param(
    [Parameter(Mandatory=$true,Position=0,HelpMessage="NSX Manager IP or FQDN")]
    [ValidateNotNullorEmpty()]
    [String]$NSXManager,
    [Parameter(Mandatory=$false,Position=1,HelpMessage="NSX Manager credentials with administrative authority")]
    [pscredential]$NSXCredentials,
    [Parameter(Mandatory=$true,Position=2,HelpMessage="Path to your JSON configuration file")]
    [ValidateNotNullorEmpty()]
    [String]$JSONPath
    )

$ErrorActionPreference = "Stop"
try
{
    if ($global:DefaultNSXConnection -eq $null)
    {
        #
        # Get NSX Manager credentials
        #
        if ($NSXCredentials -eq $null)
        {
            $NSXCredentials = Get-StoredCredential -Name nsxadmin -Message "Enter NSX Manager credentials with administrative authority" -UserName "admin"
        }
        #
        # Get vCenter credentials
        #
        if ($vCenterCredentials -eq $null)
        {
            $vCenterCredentials = Get-StoredCredential -Name vcenter -Message "Enter vCenter credentials with administrative authority" -UserName "administrator@vsphere.local"
        }
        $dummy = Connect-NsxServer -Server $NSXManager -Credential $NSXCredentials -VICred $vCenterCredentials
    }
    #
    # Parse configuration from json file
    #
    $config = Get-Content -Raw -Path $jsonpath | ConvertFrom-Json
    if ($config)
    {
        Write-Host -BackgroundColor:Black -ForegroundColor:Yellow "Status: Parsed configuration from json file."
    }
    else
    {
        throw "I don't have a config, something went wrong."
    }
    $transportZone = Get-NsxTransportZone
    if ($transportZone -eq $null)
    {
        throw "No network scope found."
    }
    #
    # Combine switches and transit into a build list (easier than messing with a custom PS object!)
    #
    $switchlist = @()
    foreach ($switch in $config.switches)
    {
        $switchlist += $switch.name
    }
    $switchlist += $config.transit.name
    #
    # Remove edge
    #
    $edge = Get-NsxEdge -Name $config.edge.name
    if ($edge -ne $null)
    {
        $edge | Remove-NsxEdge -Confirm:$false
        Write-Host -BackgroundColor Black -ForegroundColor Green "Status: Successfully deleted $($edge.name) edge."
    }
    else
    {

        Write-Host -BackgroundColor:Black -ForegroundColor White "Status: edge $($config.edge.name) not found."
    }
    #
    # Remove router
    #
    $router = Get-NsxLogicalRouter -Name $config.router.name
    if ($router -ne $null)
    {
        $router | Remove-NsxLogicalRouter -Confirm:$false
        Write-Host -BackgroundColor:Black -ForegroundColor:Green "Status: Successfully deleted $($router.name) router."
    }
    else
    {
        Write-Host -BackgroundColor:Black -ForegroundColor White "Status: router $($config.router.name) not found."
    }
    #
    # Wait for the deletes
    #
    if (($edge -ne $null) -or ($router -ne $null))
    {
        Write-Host -BackgroundColor:Black -ForegroundColor:Yellow "Status: Giving NSX Manager some time to update internal port mappings."
        Sleep 10
    }
    #
    # Remove switches
    #
    foreach ($switchEntry in $switchlist)
    {
        $switch = Get-NsxLogicalSwitch -Name $switchEntry
        if ($switch -ne $null)
        {
            $switch | Remove-NsxLogicalSwitch -Confirm:$false
            Write-Host -BackgroundColor:Black -ForegroundColor:Green "Status: Successfully deleted $switchEntry switch."
        }
        else
        {
            Write-Host -BackgroundColor:Black -ForegroundColor White "Status: switch $switchEntry not found."
        }
    }
}
catch
{
    "Something went wrong:"
    $error[0]
}
