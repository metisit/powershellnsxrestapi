Add-Type @"
    using System.Collections.Generic;
    using System.Net;
    using System.Net.Security;
    using System.Security.Cryptography.X509Certificates;
 
    public static class SSLValidator
    {
        private static Stack funcs = new Stack();
 
        private static bool OnValidateCertificate(object sender, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors)
        {
            return true;
        }
 
        public static void OverrideValidation()
        {
            funcs.Push(ServicePointManager.ServerCertificateValidationCallback);
            ServicePointManager.ServerCertificateValidationCallback = OnValidateCertificate;
        }
 
        public static void RestoreValidation()
        {
            if (funcs.Count > 0)
            {
                ServicePointManager.ServerCertificateValidationCallback = funcs.Pop();
            }
        }
    }
"@

$ErrorActionPreference = "Stop"
$baseURI = "https://nsxmanager"
try
{
    $credentials = Get-Credential -UserName "admin" -Message "Enter administrator credentials for NSX Manager"
    $nsxCredentials = New-Object System.Management.Automation.PSCredential $credentials.UserName, $credentials.Password
    $authorizationValue = [System.Convert]::ToBase64String([System.Text.Encoding]::UTF8.GetBytes($credentials.UserName + ":" + $nsxCredentials.GetNetworkCredential().password))
    $headers = New-Object "System.Collections.Generic.Dictionary[[String],[String]]"
    $headers.Add("Authorization", "Basic $authorizationValue")
    $headers.Add("Accept", "application/xml")
    $headers.Add("Content-Type", "application/xml")
    $URI = $baseURI + "/api/2.0/vdn/scopes"
    [SSLValidator]::OverrideValidation()
    $result = Invoke-WebRequest -Method Get -Uri $URI -Headers $headers -ErrorAction Stop
    [SSLValidator]::RestoreValidation()
    "De status is $($result.StatusCode), oftewel: $($result.StatusDescription)"
    [xml]$resultXML = $result.Content
    foreach ($scope in $resultXML.vdnScopes.vdnScope)
    {
        "Transport zone: $($scope.name)"
        "    Description       : $($scope.description)"
        "    # Switches        : $($scope.virtualWireCount)"
        "    Control plane mode: $($scope.controlPlaneMode)"
        ""
        [string]::Format("    {0,-20}{1,-20}", "Datacenter", "Cluster")
        "    ----------------------------------------"
        foreach ($cluster in $scope.clusters.cluster.cluster)
        {
            [string]::Format("    {0,-20}{1,-20}", $cluster.scope.name, $cluster.name)
        }
    }
}
catch
{
    "Something went wrong:"
    $error[0]
}