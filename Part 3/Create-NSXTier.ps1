#Requires -Version 4.0

<#  
.SYNOPSIS
    Creates a virtual network tier for VMware NSX
.DESCRIPTION
    Creates a virtual network tier for VMware NSX
.NOTES
    Author: Theo Hardendood, Metis IT B.V.

    Version History:
    1.0 - 05-04-2016 - Initial release
    Based on a script by Chris Wahl, @ChrisWahl, WahlNetwork.com
    Changes made on the original script:
        Replaced depricated code for handling certificate errors
        Fixed OSPF Routing
        Changed formatting of code to comply with my own coding standards
.PARAMETER NSXManager
	NSX Manager IP or FQDN
.PARAMETER NSXCredentials
	NSX Manager credentials with administrative authority
.PARAMETER vCenter
	vCenter Server IP or FQDN
.PARAMETER UseCurrentCredentials
    Use your current login credentials for vCenter
.PARAMETER vCenterCredentials
    vCenter credentials with administrative authority
.PARAMETER JSONPath
    Path to your JSON configuration file
.EXAMPLE
    PS> Create-NSXTier -NSXManager nsxmgr.tld -vCenter vcenter.tld -JSONPath "c:\path\prod.json"
#>

[CmdletBinding()]
Param
(
    [Parameter(Mandatory=$true,Position=0,HelpMessage="NSX Manager IP or FQDN")]
    [ValidateNotNullorEmpty()]
    [String]$NSXManager,
    [Parameter(Mandatory=$false,Position=1,HelpMessage="NSX Manager credentials with administrative authority")]
    [pscredential]$NSXCredentials,
    [Parameter(Mandatory=$true,Position=2,HelpMessage="vCenter Server IP or FQDN")]
    [ValidateNotNullorEmpty()]
    [String]$vCenter,
    [Parameter(Mandatory=$false,Position=3,HelpMessage="vCenter credentials with administrative authority")]
    [pscredential]$vCenterCredentials,
    [Parameter(Mandatory=$false,Position=4,HelpMessage="Use your current login credentials for vCenter")]
    [Switch]$UseCurrentCredentials,
    [Parameter(Mandatory=$true,Position=5,HelpMessage="Path to your JSON configuration file")]
    [ValidateNotNullorEmpty()]
    [String]$JSONPath
)

#
# Create custom class to handle untrusted SSL certs
#
Add-Type -Language CSharp @"
    using System.Collections.Generic;
    using System.Net;
    using System.Net.Security;
    using System.Security.Cryptography.X509Certificates;
 
    public static class SSLValidator
    {
        private static Stack<RemoteCertificateValidationCallback> funcs = new Stack<RemoteCertificateValidationCallback>();
 
        private static bool OnValidateCertificate(object sender, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors)
        {
            return true;
        }
 
        public static void OverrideValidation()
        {
            funcs.Push(ServicePointManager.ServerCertificateValidationCallback);
            ServicePointManager.ServerCertificateValidationCallback = OnValidateCertificate;
        }
 
        public static void RestoreValidation()
        {
            if (funcs.Count > 0)
            {
                ServicePointManager.ServerCertificateValidationCallback = funcs.Pop();
            }
        }
    }
"@

$ErrorActionPreference = "Stop"
try
{
    [SSLValidator]::OverrideValidation()
    #
    # Get NSX Manager credentials
    #
    if ($NSXCredentials -eq $null)
    {
        $NSXCredentials = Get-Credential -UserName "admin" -Message "Enter NSX Manager credentials with administrative authority"
    }
    $credentials = New-Object System.Management.Automation.PSCredential $NSXCredentials.UserName, $NSXCredentials.Password
    $authorizationValue = [System.Convert]::ToBase64String([System.Text.Encoding]::UTF8.GetBytes($NSXCredentials.UserName + ":" + $credentials.GetNetworkCredential().password))
    $headers = New-Object "System.Collections.Generic.Dictionary[[String],[String]]"
    $headers.Add("Authorization", "Basic $authorizationValue")
    $headers.Add("Accept", "application/xml")
    $headers.Add("Content-Type", "application/xml")
    #
    # Time this puppy!
    #
    $startClock = (Get-Date)
    #
    # Plugins and version check
    #
    try
    {
        Import-Module VMware.VimAutomation.Vds
        Write-Host -ForegroundColor Yellow -BackgroundColor Black "Status: PowerCLI version 6.0+ found."
    }
    catch
    {
        try
        {
            Add-PSSnapin VMware.VimAutomation.Vds
        }
        catch
        {
            throw "You are missing the VMware.VimAutomation.Vds snapin"
        }
        Write-Host -ForegroundColor Yellow -BackgroundColor Black "Status: PowerCLI prior to version 6.0 found."
    }
    #
    # Parse configuration from json file
    #
    try
    {
        $config = Get-Content -Raw -Path $JSONPath | ConvertFrom-Json
        Write-Host -BackgroundColor Black -ForegroundColor Yellow "Status: Parsed configuration from json file."
    }
    catch
    {
        throw "I don't have a config, something went wrong."
    }
    #
    # Combine switches and transit into a build list (easier than messing with a custom PS object!)
    #
    $switchlist = @()
    foreach ($switch in $config.switches)
    {
        $switchlist += $switch.name
    }
    $switchlist += $config.transit.name
    #
    # Get Managed Object References (MoRefs) from vCenter
    #
    if ($UseCurrentCredentials -eq $false)
    {
        if ($vCenterCredentials -eq $null)
        {
            $vCenterCredentials = Get-Credential -Message "vCenter Server credentials" -UserName "administrator@vsphere.local"
        }
        Connect-VIServer -Server $vCenter -Credential $vCenterCredentials | Out-Null
    }
    else
    {
        Connect-VIServer -Server $vCenter | Out-Null
    }
    $moref = @{}
    $moref.Add("datacenter",(Get-Datacenter $config.vsphere.datacenter | Get-View).MoRef.Value)
    $moref.Add("cluster",(Get-Cluster $config.vsphere.cluster | Get-View).MoRef.Value)
    $moref.Add("resourcepool",(Get-ResourcePool -Location (Get-Cluster $config.vsphere.cluster) -Name $config.vsphere.resourcepool | Get-View).MoRef.Value)
    $moref.Add("datastore",(Get-Datastore -Location (Get-Datacenter $config.vsphere.datacenter) $config.vsphere.datastore | Get-View).MoRef.Value)
    $moref.Add("folder",(Get-Folder $config.vsphere.folder | Get-View).MoRef.Value)
    $moref.Add("edge_uplink",(Get-VDPortgroup $config.edge.uplink.iface | Get-View).MoRef.Value)
    $moref.Add("edge_mgmt",(Get-VDPortgroup $config.edge.management.iface | Get-View).MoRef.Value)
    $moref.Add("router_mgmt",(Get-VDPortgroup $config.router.management.iface | Get-View).MoRef.Value)
    if ($moref)
    {
        Write-Host -BackgroundColor Black -ForegroundColor Yellow "Status: Gathered MoRef IDs from $vcenter."
        Disconnect-VIServer -Confirm:$false
    }
    else
    {
        throw "I don't have any MoRefs, something went wrong."
    }
    #
    # Set base URI and paging
    #
    $baseURI = "https://$NSXManager"
    $paging = "?pagesize=9999&startindex=0"
    #
    # Get the network scope (transport zone)
    # Note: I'm assuming your TZ is attached to the correct clusters
    #
    $result = Invoke-WebRequest -Uri "$baseURI/api/2.0/vdn/scopes$paging" -Headers $headers
    [xml]$xmlResult = $result.Content
    if (-not $xmlResult.vdnScopes.vdnScope.objectId)
    {
        throw "No network scope found. Create a transport zone and attach to your cluster."
    }
    $nsxScopeId = $xmlResult.vdnScopes.vdnScope.objectId
    #
    ###################
    # Create switches #
    ###################
    #
    # Get current list of switches to find if any already exist
    # Note: a switch is called a Virtual Wire in NSX
    #
    $result = Invoke-WebRequest -Uri "$baseURI/api/2.0/vdn/virtualwires$paging" -Headers $headers
    [xml]$xmlResult = $result.Content
    $switches = @()
    foreach ($virtualWire in $xmlResult.virtualWires.dataPage.virtualWire)
    {
        $switches += $virtualWire.name
    }
    #
    # Loop through our build list from earlier
    #
    foreach ($virtualWire in $switchlist)
    {
        #
        # Skip any duplicate switches
        #
        if ($switches.Contains($virtualWire))
        {
            Write-Host -BackgroundColor Black -ForegroundColor White "Warning: $virtualWire exists. Skipping."
        }
        else
        {
            #
            # Build any missing switch
            #
            [xml]$body = @"
                <virtualWireCreateSpec>
                    <name>$virtualWire</name>
                    <tenantId></tenantId>
                </virtualWireCreateSpec>
"@
            $postURI = "$baseURI/api/2.0/vdn/scopes/$nsxScopeId/virtualwires"
            $result = Invoke-WebRequest -Uri $postURI -Body $body -Method Post -Headers $headers -TimeoutSec 30
            if ($result.StatusDescription -match "Created")
            {
                Write-Host -BackgroundColor Black -ForegroundColor Green "Status: Successfully created $virtualWire switch."
            }
            else
            {
                throw "Was not able to create switch. API status description was not `"created`""
            }
        }
    }
    Write-Host -BackgroundColor Black -ForegroundColor Yellow "Status: Switch section completed."
    #
    #####################################
    # Create Distributed Logical Router #
    #####################################
    # Make sure the DLR doesn't already exist
    #
    $makeRouter = $true
    $result = Invoke-WebRequest -Uri "$baseURI/api/4.0/edges$paging" -Headers $headers
    [xml]$xmlResult = $result.Content
    foreach ($router in $xmlResult.pagedEdgeList.edgePage.edgeSummary)
    {
        if ($router.name -eq $config.router.name)
        {
            Write-Host -BackgroundColor Black -ForegroundColor White "Warning: $($router.name) exists. Skipping."
            $makeRouter = $false
        }
    }
    #
    # Get virtualwire ID for switches
    # Note: We can't assume that this script built the switches above, so let's query the API again now that all switches exist
    #
    $result = Invoke-WebRequest -Uri "$baseURI/api/2.0/vdn/virtualwires$paging" -Headers $headers
    [xml]$xmlResult = $result.Content
    $switchVirtualWire = @{}
    foreach ($virtualWire in $xmlResult.virtualWires.dataPage.virtualWire)
    {
        $switchVirtualWire.Add($virtualWire.name,$virtualWire.objectId)
    }
    #
    # If the $makeRouter flag is $false, skip the router part. Using it as a flag based on the previous router search.
    #
    If ($makeRouter -ne $false)
    {
        #
        # Start a new body for the router XML payload
        #
        [string]$body = @"
            <edge>
                <datacenterMoid>$($moref.datacenter)</datacenterMoid>
                <name>$($config.router.name)</name>
                <fqdn>$($config.router.name)</fqdn>
                <tenant>$($config.router.tenant)</tenant>
                <appliances>
                    <applianceSize>compact</applianceSize>
                    <appliance>
                        <resourcePoolId>$($moref.resourcepool)</resourcePoolId>
                        <datastoreId>$($moref.datastore)</datastoreId>
                        <vmFolderId>$($moref.folder)</vmFolderId>
                    </appliance>
                </appliances>
                <cliSettings>
                    <remoteAccess>$($config.router.cli.enabled)</remoteAccess>
                    <userName>$($config.router.cli.user)</userName>
                    <password>$($config.router.cli.pass)</password>
                    <passwordExpiry>$($config.router.cli.expiredays)</passwordExpiry>
                </cliSettings>
                <type>distributedRouter</type>
                <mgmtInterface>
                    <label>vNic_0</label>
                    <name>mgmtInterface</name>
                    <addressGroups />
                    <mtu>1500</mtu>
                    <index>0</index>
                    <connectedToId>$($moref.router_mgmt)</connectedToId>
                </mgmtInterface>
                <features>
                    <firewall>
                        <enabled>$($config.router.firewall)</enabled>
                    </firewall>
                    <highAvailability>
                        <enabled>$($config.router.ha)</enabled>
                    </highAvailability>
                </features>
"@
        #
        # Add the uplink interface
        #
        $body += @"
                <interfaces>
                    <interface>
                        <name>$($config.transit.name)</name>
                        <type>uplink</type>
                        <mtu>1500</mtu>
                        <isConnected>true</isConnected>
                        <addressGroups>
                            <addressGroup>
                                <primaryAddress>$($config.transit.routerip)</primaryAddress>
                                <subnetMask>$($config.transit.mask)</subnetMask>
                            </addressGroup>
                        </addressGroups>
                        <connectedToId>$($switchVirtualWire.get_Item($config.transit.name))</connectedToId>
                    </interface>
"@
        # 
        # Add the internal interface(s) via a loop
        #
        foreach ($switch in $config.switches)
        {
            $body += @"
                    <interface>
                        <name>$($switch.name)</name>
                        <type>internal</type>
                        <mtu>1500</mtu>
                        <isConnected>true</isConnected>
                        <addressGroups>
                            <addressGroup>
                                <primaryAddress>$($switch.ip)</primaryAddress>
                                <subnetMask>$($switch.mask)</subnetMask>
                            </addressGroup>
                        </addressGroups>
                        <connectedToId>$($switchVirtualWire.get_Item($switch.name))</connectedToId>
                    </interface>
"@
        }
        #
        # Close XML tags
        #
        $body += @"
                </interfaces>
            </edge>
"@
        #
        # Post the router to the API
        # Note: At this point, no routing is configured. It appears the API wants that after the build is done and not before.
        #
        Write-Host -BackgroundColor Black -ForegroundColor Yellow "Status: Creating router. This may take a few minutes."
        $result = Invoke-WebRequest -Uri "$baseURI/api/4.0/edges" -Body $body -Method Post -Headers $headers -TimeoutSec 180
        if ($result.StatusDescription -match "Created")
        {
            Write-Host -BackgroundColor Black -ForegroundColor Green "Status: Successfully created $($config.router.name) router."
            $routerid = ($result.Headers.get_Item("Location")).split("/") | Select-Object -Last 1
        }
        else
        {
            $body
            throw "Was not able to create router. API status description was not `"created`""
        }
        #
        # Routing configuration
        #
        $body = @"
            <routing>
                <enabled>true</enabled>
                <routingGlobalConfig>
                    <routerId>$($config.transit.routerip)</routerId>
                    <logging>
                        <enable>true</enable>
                        <logLevel>warning</logLevel>
                    </logging>
                </routingGlobalConfig>
                <staticRouting>
                    <defaultRoute>
                        <vnic>2</vnic>
                        <mtu>1500</mtu>
                        <gatewayAddress>$($config.transit.edgeip)</gatewayAddress>
                    </defaultRoute>
                </staticRouting>
                <ospf>
                    <enabled>$($config.router.ospf.enabled)</enabled>
                    <protocolAddress>$($config.transit.protoip)</protocolAddress>
                    <forwardingAddress>$($config.transit.routerip)</forwardingAddress>
                    <ospfAreas>
                        <ospfArea>
                            <areaId>$($config.router.ospf.area)</areaId>
                            <type>$($config.router.ospf.type)</type>
                            <authentication>
                                <type>none</type>
                            </authentication>
                        </ospfArea>
                    </ospfAreas>
                    <ospfInterfaces>
                        <ospfInterface>
                            <vnic>2</vnic>
                            <areaId>$($config.router.ospf.area)</areaId>
                            <helloInterval>10</helloInterval>
                            <deadInterval>40</deadInterval>
                            <priority>128</priority>
                            <cost>1</cost>
                            <mtuIgnore>false</mtuIgnore>
                        </ospfInterface>
                    </ospfInterfaces>
                    <redistribution>
                        <enabled>true</enabled>
                        <rules>
                            <rule>
                                <id>0</id>
                                <from>
                                    <isis>false</isis>
                                    <ospf>false</ospf>
                                    <bgp>false</bgp>
                                    <static>false</static>
                                    <connected>true</connected>
                                </from>
                                <action>permit</action>
                            </rule>
                        </rules>
                    </redistribution>
                    <gracefulRestart>true</gracefulRestart>
                    <defaultOriginate>false</defaultOriginate>
                </ospf>
            </routing>
"@
        #
        # Configure routing on the router
        #
        $result = Invoke-WebRequest -Uri "$baseURI/api/4.0/edges/$routerid/routing/config" -Body $body -Method Put -Headers $headers -TimeoutSec 30
        if ($result.StatusCode -match "204")
        {
            Write-Host -BackgroundColor Black -ForegroundColor Green "Status: Successfully applied routing config to $($config.router.name)."
        }
        else
        {
            $body
            throw "Was not able to apply routing config to router. API status code was not 204."
        }
    }
    Write-Host -BackgroundColor Black -ForegroundColor Yellow "Status: Router section completed."
    #
    ###############
    # Create edge #
    ###############
    #
    # Make sure the edge doesn't already exist
    #
    $makeEdge = $true
    $result = Invoke-WebRequest -Uri "$baseURI/api/4.0/edges$paging" -Headers $headers
    [xml]$xmlResult = $result.Content
    foreach ($edge in $xmlResult.pagedEdgeList.edgePage.edgeSummary)
    {
        if ($edge.name -eq $config.edge.name)
        {
            Write-Host -BackgroundColor Black -ForegroundColor White "Warning: $($edge.name) exists. Skipping."
            $makeEdge = $false
        }
    }
    #
    # If the $makerouter flag is $false, skip the router part. Using it as a flag based on the previous router search.
    #
    If ($makeEdge -ne $false)
    {
        #
        # Start a new body for the edge XML payload (bleh to XML!)
        #
        [string]$body = @"
            <edge>
                <datacenterMoid>$($moref.datacenter)</datacenterMoid>
                <name>$($config.edge.name)</name>
                <fqdn>$($config.edge.name)</fqdn>
                <tenant>$($config.edge.tenant)</tenant>
                <vseLogLevel>emergency</vseLogLevel>
                <vnics>
                    <vnic>
                        <label>vNic_0</label>
                        <name>$($config.edge.uplink.name)</name>
                        <addressGroups>
                            <addressGroup>
                                <primaryAddress>$($config.edge.uplink.ip)</primaryAddress>
                                <subnetMask>$($config.edge.uplink.mask)</subnetMask>
                            </addressGroup>
                        </addressGroups>
                        <mtu>1500</mtu>
                        <type>uplink</type>
                        <isConnected>true</isConnected>
                        <index>0</index>
                        <portgroupId>$($moref.edge_uplink)</portgroupId>
                        <enableProxyArp>false</enableProxyArp>
                        <enableSendRedirects>true</enableSendRedirects>
                    </vnic>
                    <vnic>
                        <label>vNic_1</label>
                        <name>$($config.transit.name)</name>
                        <addressGroups>
                            <addressGroup>
                                <primaryAddress>$($config.transit.edgeip)</primaryAddress>
                                <subnetMask>$($config.transit.mask)</subnetMask>
                            </addressGroup>
                        </addressGroups>
                        <mtu>1500</mtu>
                        <type>internal</type>
                        <isConnected>true</isConnected>
                        <index>1</index>
                        <portgroupId>$($switchVirtualWire.get_Item($config.transit.name))</portgroupId>
                        <enableProxyArp>false</enableProxyArp>
                        <enableSendRedirects>false</enableSendRedirects>
                    </vnic>
                </vnics>
                <appliances>
                    <applianceSize>compact</applianceSize>
                    <appliance>
                        <resourcePoolId>$($moref.resourcepool)</resourcePoolId>
                        <datastoreId>$($moref.datastore)</datastoreId>
                        <vmFolderId>$($moref.folder)</vmFolderId>
                    </appliance>
                </appliances>
                <cliSettings>
                    <remoteAccess>$($config.edge.cli.enabled)</remoteAccess>
                    <userName>$($config.edge.cli.user)</userName>
                    <password>$($config.edge.cli.pass)</password>
                    <passwordExpiry>$($config.edge.cli.expiredays)</passwordExpiry>
                </cliSettings>
                <features>
                    <firewall>
                        <enabled>$($config.edge.firewall)</enabled>
                    </firewall>
                    <highAvailability>
                        <enabled>$($config.edge.ha)</enabled>
                    </highAvailability>
                </features>
                <type>gatewayServices</type>
            </edge>
"@
        #
        # Post the edge to the API
        # Note: At this point, no routing is configured. It appears the API wants that after the build is done and not before.
        #
        Write-Host -BackgroundColor Black -ForegroundColor Yellow "Status: Creating edge. This may take a few minutes."
        $result = Invoke-WebRequest -Uri "$baseURI/api/4.0/edges" -Body $body -Method Post -Headers $headers -TimeoutSec 180
        if ($result.StatusDescription -match "Created")
        {
            Write-Host -BackgroundColor Black -ForegroundColor Green "Status: Successfully created $($config.edge.name) edge."
            $edgeid = ($result.Headers.get_Item("Location")).split("/") | Select-Object -Last 1
        }
        else
        {
            $body
            throw "Was not able to create edge. API status description was not `"created`""
        }
        #
        # Routing configuration
        #
        $body = @"
            <routing>
                <enabled>true</enabled>
                <routingGlobalConfig>
                    <routerId>$($config.edge.uplink.ip)</routerId>
                    <logging>
                        <enable>true</enable>
                        <logLevel>warning</logLevel>
                    </logging>
                </routingGlobalConfig>
                <staticRouting>
                    <defaultRoute>
                        <vnic>0</vnic>
                        <mtu>1500</mtu>
                        <gatewayAddress>$($config.edge.uplink.gateway)</gatewayAddress>
                    </defaultRoute>
                </staticRouting>
                <ospf>
                    <enabled>$($config.edge.ospf.enabled)</enabled>
                    <ospfAreas>
                        <ospfArea>
                            <areaId>$($config.edge.ospf.area)</areaId>
                            <type>$($config.edge.ospf.type)</type>
                        </ospfArea>
                        <ospfArea>
                            <areaId>0</areaId>
                            <type>normal</type>
                        </ospfArea>
                    </ospfAreas>
                    <ospfInterfaces>
"@
        #
        # Configure OSPF on the Uplink (north) interface, vNIC0
        #
        if ($($config.edge.ospf.uplink) -eq "true")
        {
            $body += @"
                        <ospfInterface>
                            <vnic>0</vnic>
                            <areaId>0</areaId>
                            <helloInterval>10</helloInterval>
                            <deadInterval>40</deadInterval>
                            <priority>128</priority>
                            <cost>1</cost>
                            <mtuIgnore>false</mtuIgnore>
                        </ospfInterface>
"@
        }
        #
        # Configure OSPF on the Internal (south) interface, vNIC1
        #
        if ($($config.edge.ospf.internal) -eq "true")
        {
            $body += @"
                        <ospfInterface>
                            <vnic>1</vnic>
                            <areaId>$($config.edge.ospf.area)</areaId>
                            <helloInterval>10</helloInterval>
                            <deadInterval>40</deadInterval>
                            <priority>128</priority>
                            <cost>1</cost>
                            <mtuIgnore>false</mtuIgnore>
                        </ospfInterface>
"@
        }

        $body += @"
                    </ospfInterfaces>
                    <redistribution>
                        <enabled>true</enabled>
                        <rules>
                            <rule>
                                <id>0</id>
                                <from>
                                    <isis>false</isis>
                                    <ospf>false</ospf>
                                    <bgp>false</bgp>
                                    <static>false</static>
                                    <connected>true</connected>
                                </from>
                                <action>permit</action>
                            </rule>
                        </rules>
                    </redistribution>
                    <gracefulRestart>true</gracefulRestart>
                    <defaultOriginate>false</defaultOriginate>
                </ospf>
            </routing>
"@
        #
        # Configure routing on the edge
        #
        $result = Invoke-WebRequest -Uri "$baseURI/api/4.0/edges/$edgeid/routing/config" -Body $body -Method Put -Headers $headers -TimeoutSec 30
        if ($result.StatusCode -match "204")
        {
            Write-Host -BackgroundColor Black -ForegroundColor Green "Status: Successfully applied routing config to $($config.edge.name)."
        }
        else
        {
            $body
            throw "Was not able to apply routing config to router. API status code was not 204."
        }
    }
    Write-Host -BackgroundColor Black -ForegroundColor Yellow "Status: Edge section completed."
    #
    ############
    # Complete #
    ############
    #
    $endClock = (Get-Date)
    $totalClock = [Math]::Round(($endClock-$startClock).totalseconds)
    Write-Host -BackgroundColor Black -ForegroundColor Green "Status: Environment created in $totalclock seconds"
    [SSLValidator]::RestoreValidation()
}
catch
{
    Write-Host -BackgroundColor Black -ForegroundColor Red "Status: A system exception was caught."
    Write-Host -BackgroundColor Black -ForegroundColor Red $Error[0]
}