#Requires -Version 4.0

<#  
.SYNOPSIS
    Removes a virtual network tier in VMware NSX
.DESCRIPTION
    Removes a virtual network tier in VMware NSX
.NOTES
    Author: Theo Hardendood, Metis IT B.V.

    1.0 - 05-04-2016 - Initial release
    Based on a script by Chris Wahl, @ChrisWahl, WahlNetwork.com
    Changes made on the original script:
        Replaced depricated code for handling certificate errors
        Fixed OSPF Routing
        Changed formatting of code to comply with my own coding standards
.PARAMETER NSXManager
    NSX Manager IP or FQDN
.PARAMETER NSXCredentials
    NSX Manager credentials with administrative authority
.PARAMETER JSONPath
    Path to your JSON configuration file
.EXAMPLE
    PS> Remove-NSXTier -NSX nsxmgr.tld -JSONPath "c:\path\prod.json"
#>

[CmdletBinding()]
Param(
    [Parameter(Mandatory=$true,Position=0,HelpMessage="NSX Manager IP or FQDN")]
    [ValidateNotNullorEmpty()]
    [String]$NSXManager,
    [Parameter(Mandatory=$false,Position=1,HelpMessage="NSX Manager credentials with administrative authority")]
    [pscredential]$NSXCredentials,
    [Parameter(Mandatory=$true,Position=2,HelpMessage="Path to your JSON configuration file")]
    [ValidateNotNullorEmpty()]
    [String]$JSONPath
    )

#
# Create custom class to handle untrusted SSL certs
#
Add-Type -Language CSharp @"
    using System.Collections.Generic;
    using System.Net;
    using System.Net.Security;
    using System.Security.Cryptography.X509Certificates;
 
    public static class SSLValidator
    {
        private static Stack<RemoteCertificateValidationCallback> funcs = new Stack<RemoteCertificateValidationCallback>();
 
        private static bool OnValidateCertificate(object sender, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors)
        {
            return true;
        }
 
        public static void OverrideValidation()
        {
            funcs.Push(ServicePointManager.ServerCertificateValidationCallback);
            ServicePointManager.ServerCertificateValidationCallback = OnValidateCertificate;
        }
 
        public static void RestoreValidation()
        {
            if (funcs.Count > 0)
            {
                ServicePointManager.ServerCertificateValidationCallback = funcs.Pop();
            }
        }
    }
"@

$ErrorActionPreference = "Stop"
try
{
    [SSLValidator]::OverrideValidation()
    #
    # get cedentials
    #
    if ($NSXCredentials -eq $null)
    {
        $NSXCredentials = Get-Credential -UserName "admin" -Message "Enter NSX Manager credentials with administrative authority"
    }
    $credentials = New-Object System.Management.Automation.PSCredential $NSXCredentials.UserName, $NSXCredentials.Password
    $authorizationValue = [System.Convert]::ToBase64String([System.Text.Encoding]::UTF8.GetBytes($NSXCredentials.UserName + ":" + $credentials.GetNetworkCredential().password))
    $headers = New-Object "System.Collections.Generic.Dictionary[[String],[String]]"
    $headers.Add("Authorization", "Basic $authorizationValue")
    $headers.Add("Accept", "application/xml")
    $headers.Add("Content-Type", "application/xml")
    #
    # Parse configuration from json file
    #
    $config = Get-Content -Raw -Path $jsonpath | ConvertFrom-Json
    if ($config)
    {
        Write-Host -BackgroundColor:Black -ForegroundColor:Yellow "Status: Parsed configuration from json file."
    }
    else
    {
        throw "I don't have a config, something went wrong."
    }
    #
    # Combine switches and transit into a build list (easier than messing with a custom PS object!)
    #
    $switchlist = @()
    foreach ($switch in $config.switches)
    {
        $switchlist += $switch.name
    }
    $switchlist += $config.transit.name
    #
    # Set base URI and paging
    #
    $baseURI = "https://$NSXManager"
    $paging = "?pagesize=9999&startindex=0"
    #
    # Remove edge
    #
    $foundEdge = $false
    $result = Invoke-WebRequest -Uri "$baseURI/api/4.0/edges$paging" -Headers $headers -ContentType "application/xml"
    [xml]$xmlResult = $result.Content
    foreach ($edge in $xmlResult.pagedEdgeList.edgePage.edgeSummary)
    {
        if ($edge.name -eq $config.edge.name)
        {
            $foundEdge = $true
            $result = Invoke-WebRequest -Uri "$baseURI/api/4.0/edges/$($edge.objectId)" -Headers $headers -Method Delete
            if ($result.StatusCode -eq "204")
            {
                Write-Host -BackgroundColor Black -ForegroundColor Green "Status: Successfully deleted $($edge.name) edge."
            }
        }
    }
    if ($foundEdge -eq $false)
    {
        Write-Host -BackgroundColor:Black -ForegroundColor White "Status: edge $($config.edge.name) not found."
    }
    #
    # Remove router
    #
    $foundRouter = $false
    $result = Invoke-WebRequest -Uri "$baseURI/api/4.0/edges$paging" -Headers $headers
    [xml]$xmlResult = $result.Content
    foreach ($router in $xmlResult.pagedEdgeList.edgePage.edgeSummary)
    {
        if ($router.name -eq $config.router.name)
        {
            $foundRouter = $true
            $result = Invoke-WebRequest -Uri "$baseURI/api/4.0/edges/$($router.objectId)" -Headers $headers -Method Delete
            if ($result.StatusCode -eq "204")
            {
                Write-Host -BackgroundColor:Black -ForegroundColor:Green "Status: Successfully deleted $($router.name) router."
            }
        }
    }
    if ($foundRouter -eq $false)
    {
        Write-Host -BackgroundColor:Black -ForegroundColor White "Status: edge $($config.router.name) not found."
    }
    #
    # Wait for the deletes
    #
    if ($foundEdge -or $foundRouter)
    {
        Write-Host -BackgroundColor:Black -ForegroundColor:Yellow "Status: Giving NSX Manager some time to update internal port mappings."
        Sleep 10
    }
    #
    # Remove switches
    #
    $switchesFound = @()
    $result = Invoke-WebRequest -Uri "$baseURI/api/2.0/vdn/virtualwires$paging" -Headers $headers
    [xml]$xmlResult = $result.Content
    foreach ($switch in $xmlResult.virtualWires.dataPage.virtualWire)
    {
        if ($switchlist -contains $switch.name)
        {
            $switchesFound += $switch.name
            $result = Invoke-WebRequest -Uri "$baseURI/api/2.0/vdn/virtualwires/$($switch.objectId)" -Headers $headers -Method Delete
            if ($result.StatusCode -eq "200")
            {
                Write-Host -BackgroundColor:Black -ForegroundColor:Green "Status: Successfully deleted $($switch.name) switch."
            }
        }
    }
    foreach ($switch in $switchlist)
    {
        if ($switchesFound -notcontains $switch)
        {
            Write-Host -BackgroundColor:Black -ForegroundColor White "Status: switch $switch not found."
        }
    }
    [SSLValidator]::RestoreValidation()
}
catch
{
    "Something went wrong:"
    $error[0]
}
