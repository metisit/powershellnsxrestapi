# PowerShellNSXRestAPI #

This repository contains the source code of the blog serie [Software Defined Networking with PowerShell and the VMware NSX REST API](http://www.metisit.com/software-defined-networking-powershell-vmware-nsx-rest-api/).

This blog serie is written in Dutch, but an English version will be available soon.

* Part 1 is browser-based, so no source code is available.
* Part 2 is a simple NSX ReST query (Show NSX Transport Zones)
* Part 3 creates a working 3-tier network with OSPF routing configured.